---
layout: post
title: "开始写博客了"
author: "ranke"
categories: documentation
tags: [documentation,sample]
image: oldhome1.jpg
---

# gitee pages 搭建博客

前几天就搭好了博客，由于github被屏蔽了，就暂时考虑在码云上写。

![alt text](https://ranke110.gitee.io/assets/img/oldhome2.jpg "老家")

## 本年目标

今年还有一些小目标没有完成，希望能在年前能够达成目标。

### JAVA学习
在上班的时候，偶尔学一些spring上的各种操作。现在大部分项目都是spring boot起手，很简单的就搭好了基础框架。我暂时只学习了spring boot 的helloworld。最终能够实现用springboot + mybatis + mysql + kendoUI搭建一个简单的系统，算是掌握基础实现逻辑。以后有机会再学一点开发板上的java for armhf，然后把家里的开发板用起来。

#### 又是想念松糕的一天
![alt text](https://ranke110.gitee.io/assets/img/songgao.jpg "松糕")