---
layout: post
title: "Sqlserver 还原网络共享文件"
author: "ranke"
categories: documentation
tags: [documentation,sample]
image: sqlserver.jpg
---

#sqlserver还原网络共享文件

###0. 需要使用Sqlcmd来执行
在命令行输入sqlcmd。

###1. 打开xp_cmdshell 用来添加网络驱动器访问权限:
```sql
exec sp_configure 'show advanced options',1
go
reconfigure
go
```

###2. 将xp_cmdshell 权限打开
```sql
exec sp_configure 'xp_cmdshell',1
go
reconfigure
go
```

###3. 增加sqlserver 对共享路径的访问权限:
```sql
exec master..xp_cmdshell 'net use Y: \\192.168.10.100\temp mplh@2020 /user:192.168.10.100\Administrator'
go
reconfigure
go
```